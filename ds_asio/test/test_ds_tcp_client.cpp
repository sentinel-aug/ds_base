//
// Created by ivaughn on 5/19/21.
//

#include <gtest/gtest.h>

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include "ds_asio/ds_tcp_client.h"

struct TestStats {
  TestStats() {
    reset();
  }

  void reset() {
    msg_recv = 0;
    conn_recv = 0;
    conn_quit = 0;
    errors = 0;
  }

  size_t msg_recv;
  size_t conn_recv;
  size_t conn_quit;
  size_t errors;

  void printStats() const {
    ROS_INFO_STREAM(" TCPSERVER STATS: \n"
              << "    msg: " << msg_recv << "\n"
              << "   recv: " << conn_recv << "\n"
              << "   quit: " << conn_quit << "\n"
              << "    err: " << errors << "\n");
  }

  std::string strStats() const {
    std::stringstream ret;
    ret << msg_recv << " " << conn_recv << " " <<conn_quit << " "<< errors << "\n";
    return ret.str();
  }

};
class TestTcpSession {
 public:
  TestTcpSession(boost::asio::io_service& io, TestStats* s) : ios_(io), socket_(new boost::asio::ip::tcp::socket(io)), stats(s) {
  }

  virtual ~TestTcpSession() {
    if (socket_ != nullptr) {
      delete socket_;
      socket_ = nullptr;
    }
  }

  boost::asio::ip::tcp::socket& socket() {
    if (socket_ == nullptr) {
      throw std::runtime_error("Attempting to re-use an already memory-leaked socket");
    }
    return *socket_;
  }

  void start() {
    socket_->async_read_some(boost::asio::buffer(data_, max_len_),
                            boost::bind(&TestTcpSession::handle_read, this, boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred()));
  }

 protected:
  boost::asio::io_service& ios_;
  boost::asio::ip::tcp::socket* socket_;
  const static size_t max_len_ = 1024;
  char data_[max_len_];

  TestStats* stats;

  void handle_read(const boost::system::error_code& error, size_t bytes_transferred) {
    if (error) {
      stats->errors++;
      ROS_ERROR_STREAM("TCPSERVER_CONN had an error!: " <<error);
      stats->conn_quit++;
      delete this;
      return;
    }

    ROS_INFO_STREAM("TCPSERVER_CONN Got: " <<bytes_transferred <<" bytes!");
    std::string recv(data_, bytes_transferred);
    ROS_INFO_STREAM("GOT: \"" <<recv <<"\"");
    stats->msg_recv++;
    if (recv[0] == 0x04 || recv.substr(0, 4) == "EXIT") {
      std::cout << "Disconnected!" << std::endl;
      boost::system::error_code ec;
      socket_->shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
      socket_->close();
      stats->conn_quit++;
      delete this;
      return;
    } else if (recv.substr(0, 4) == "DROP") {
      // same as above, but drop the connection grather than closing it gracefully
      std::cout <<"Disconnected! (drop)" <<std::endl;
      // don't close; instead, we're going to leak the socket on purpose.  This leaves it floating around, never
      // de-allocated, force to wander the process's allocated file descriptors until, at exit, the kernel grants the
      // socket the sweet embrace of proper cleanup.
      socket_->cancel();
      socket_ = nullptr;
      stats->conn_quit++;
      delete this;
      return;
    } else if (recv.substr(0, 3) == "DIE") {
      std::cout <<"Disconnected!" <<std::endl;
      stats->conn_quit++;
      ios_.stop();
      delete this;
      return;
    } else if (recv.substr(0,5) == "STATS") {
      std::string statstr = stats->strStats();
      statstr.copy(data_, max_len_);
      bytes_transferred = std::min(statstr.length(), max_len_-1);
      data_[bytes_transferred] = '\0';
    }

    boost::asio::async_write(*socket_, boost::asio::buffer(data_, bytes_transferred),
                             boost::bind(&TestTcpSession::handle_write, this,
                                         boost::asio::placeholders::error));
  }

  void handle_write(const boost::system::error_code& error) {
    if (!error) {
      socket_->async_read_some(boost::asio::buffer(data_, max_len_),
                              boost::bind(&TestTcpSession::handle_read, this, boost::asio::placeholders::error,
                                          boost::asio::placeholders::bytes_transferred()));
    } else {
      ROS_ERROR_STREAM("ERROR in server connection! " <<error);
      stats->errors++;
      delete this;
    }
  }
};

class TestTcpServer {
 public:

  TestTcpServer(boost::asio::io_service& io, uint16_t port, TestStats* s) : ios_(io),
                                                                            stats(s),
                                                                            acceptor_(io,
                                                                                      boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)) {
    start_accept();
  }

  void start_accept() {
    TestTcpSession* session = new TestTcpSession(ios_, stats);
    acceptor_.async_accept(session->socket(), boost::bind(&TestTcpServer::handle_accept, this, session, boost::asio::placeholders::error));
  }

 protected:
  boost::asio::io_service& ios_;
  boost::asio::ip::tcp::acceptor acceptor_;
  TestStats* stats;

  void handle_accept(TestTcpSession* session, const boost::system::error_code& error) {
    if (!error) {
      stats->conn_recv++;
      session->start();
    } else {
      std::cout <<"ERROR establishing connection!... " <<error <<std::endl;
      stats->errors++;
      delete session;
    }

    stats->printStats();
    start_accept();
  }
};

class DsTcpClientTest : public ::testing::Test {

  void SetUp () override {
    ROS_INFO_STREAM("Starting test server...");
    test_server_ = new TestTcpServer(io_, PORT, &test_stats_);

    ROS_INFO_STREAM("Starting new thread for test TCP Server...");
    thread_ = new boost::thread(boost::bind(&boost::asio::io_service::run, &io_));

    nh_.reset(new ros::NodeHandle());  // only used for setup, so we don't need the asio hack
    underTest_.reset(new ds_asio::DsTcpClient(io_, "test_tcp", boost::bind(&DsTcpClientTest::readCallback, this, _1), *nh_));
    underTest_->setReconnectCallback(boost::bind(&DsTcpClientTest::reconnectCallback, this));

    ROS_INFO_STREAM("Ready to run tests!");

    reconnect_callback_calls_ = 0;
  }

  void TearDown() override {
    ROS_INFO_STREAM("Stopping test server...");
    io_.stop();
    ROS_INFO_STREAM("Waiting for server thread...");
    thread_->join();
    ROS_INFO_STREAM("Destroying test assets...");
    // prep for possible next run
    io_.reset(); // yes, this is deprecated. But old boost version
    delete test_server_;
    delete thread_;
    ROS_INFO_STREAM("TEST COMPLETE!");
  }

  void readCallback(ds_core_msgs::RawData data) {
    ROS_INFO_STREAM("TCP Test fixture got data!");
    last_msg_recv_ = data;
  }

  void reconnectCallback() {
    ROS_INFO_STREAM("TCP Test fixture got reconnect callback");
    reconnect_callback_calls_++;
  }

 protected:
  void waitForTraffic(int milliseconds = 100) {
    boost::asio::deadline_timer timer(io_, boost::posix_time::milliseconds(milliseconds));
    timer.wait();
  }

 protected:
  TestStats test_stats_;
  const static uint16_t PORT=6000;
  ros::NodeHandlePtr nh_;
  std::unique_ptr<ds_asio::DsTcpClient> underTest_;
  ds_core_msgs::RawData last_msg_recv_;
  int reconnect_callback_calls_;

 private:
  boost::asio::io_service io_;
  boost::thread* thread_;
  TestTcpServer* test_server_;

};

TEST_F(DsTcpClientTest, basic_connect) {

  boost::shared_ptr<std::string> to_send(new std::string("TEST MSG\n"));
  EXPECT_EQ(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(0, last_msg_recv_.data.size());

  underTest_->send(to_send);
  waitForTraffic();

  EXPECT_NE(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(ds_core_msgs::RawData::DATA_IN, last_msg_recv_.data_direction);
  EXPECT_EQ(to_send->length(), last_msg_recv_.data.size());
  std::string recv_string(last_msg_recv_.data.begin(), last_msg_recv_.data.end());
  EXPECT_EQ("TEST MSG\n", recv_string);

  EXPECT_EQ(1, test_stats_.msg_recv);
}

TEST_F(DsTcpClientTest, basic_reconnect) {
  // in this test, the server nicely tells us it's resetting the connection

  underTest_->setReconnectTime(boost::posix_time::milliseconds(50));
  EXPECT_EQ(0, reconnect_callback_calls_);
  waitForTraffic(50); // leave time for initial connection
  EXPECT_EQ(1, reconnect_callback_calls_);

  boost::shared_ptr<std::string> to_send(new std::string("EXIT\n"));
  EXPECT_EQ(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(0, last_msg_recv_.data.size());

  // to expedite this test, we want to set the reconnect time to be very speedy
  underTest_->send(to_send);
  waitForTraffic(150); // leave plenty of time to reconnect
  // should automagically reconnect
  EXPECT_EQ(2, reconnect_callback_calls_);

  to_send.reset(new std::string("TEST MSG\n"));
  EXPECT_EQ(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(0, last_msg_recv_.data.size());

  underTest_->send(to_send);
  waitForTraffic();

  EXPECT_NE(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(ds_core_msgs::RawData::DATA_IN, last_msg_recv_.data_direction);
  EXPECT_EQ(to_send->length(), last_msg_recv_.data.size());
  std::string recv_string(last_msg_recv_.data.begin(), last_msg_recv_.data.end());
  EXPECT_EQ("TEST MSG\n", recv_string);

  EXPECT_EQ(2, reconnect_callback_calls_);
  EXPECT_EQ(2, test_stats_.msg_recv);
  EXPECT_EQ(2, test_stats_.conn_recv);
}

TEST_F(DsTcpClientTest, timeout_reconnect) {
  // in this test, the server Just Dies without telling us it's dropping the connection

  underTest_->setReconnectTime(boost::posix_time::milliseconds(50));
  underTest_->setTimeout(boost::posix_time::milliseconds(250));
  EXPECT_EQ(0, reconnect_callback_calls_);
  waitForTraffic(50); // leave time for initial connection
  EXPECT_EQ(1, reconnect_callback_calls_);

  boost::shared_ptr<std::string> to_send(new std::string("DROP\n"));
  EXPECT_EQ(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(0, last_msg_recv_.data.size());

  // to expedite this test, we want to set the reconnect time to be very speedy
  underTest_->send(to_send);
  waitForTraffic(350); // leave plenty of time to reconnect
  // should automagically reconnect
  EXPECT_EQ(2, reconnect_callback_calls_);

  to_send.reset(new std::string("TEST MSG\n"));
  EXPECT_EQ(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(0, last_msg_recv_.data.size());

  underTest_->send(to_send);
  waitForTraffic();

  EXPECT_NE(0, last_msg_recv_.ds_header.io_time.toNSec());
  EXPECT_EQ(ds_core_msgs::RawData::DATA_IN, last_msg_recv_.data_direction);
  EXPECT_EQ(to_send->length(), last_msg_recv_.data.size());
  std::string recv_string(last_msg_recv_.data.begin(), last_msg_recv_.data.end());
  EXPECT_EQ("TEST MSG\n", recv_string);

  EXPECT_EQ(2, reconnect_callback_calls_);
  EXPECT_EQ(2, test_stats_.msg_recv);
  EXPECT_EQ(2, test_stats_.conn_recv);
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "ds_tcp_client");
  ros::NodeHandle nh; // remember we need to guarantee a node hand exists for the entire lifespan of this node
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
